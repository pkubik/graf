CC=g++
CFLAGS=-ggdb3 -Wall -std=c++11
LFLAGS=-g
EXECUTABLE=graf
SOURCES=Graph.cpp FibHeap.cpp
TESTSOURCES=FibHeap_test.cpp
TESTEXE=fibheap_test
MAIN=main.cpp
OBJS=$(SOURCES:.cpp=.o)
MAINOBJ=main.o

all: .Make.dep $(SOURCES) $(TESTSOURCES) $(MAIN) $(EXECUTABLE) $(TESTEXE)

-include .Make.dep

$(EXECUTABLE): $(OBJS) $(MAINOBJ)
	@echo "Budowanie $(EXECUTABLE)"
	$(CC) $(MAINOBJ) $(OBJS) $(LFLAGS) -o $(EXECUTABLE)
	
fibheap_test: $(EXECUTABLE) FibHeap_test.o
	@echo "Budowanie fibheap_test"
	$(CC) -O0 FibHeap_test.o $(OBJS) $(LFLAGS) -o fibheap_test

.cpp.o:
	@echo "Budowanie $@"
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f *.o
	rm -f ./.Make.dep
	
.Make.dep: $(SRCS)
	rm -f ./.Make.dep
	$(CC) $(CFLAGS) $(SOURCES) $(TESTSOURCES) -MM $^>>./.Make.dep