/*
* Paweł Kubik
* Politechnika Warszawska
* WEiTI
*/

#include <memory>

#ifndef FIBHEAP_H
#define FIBHEAP_H

using namespace std;

struct Visited;
struct FibHeap;

struct Node { //nie ma iteratora bo bieda w kraju
	pair<unsigned, Visited*> data;
	bool marked;
	unsigned degree;
	Node* parent;
	Node* children;
	Node* prev;
	Node* next;
	Node() : data(make_pair(-1,nullptr)), marked(false), degree(0), parent(nullptr), children(nullptr), prev(this), next(this) {} //wartownik
	Node(const pair<unsigned, Visited*>& d) : data(d), marked(false), degree(0), parent(nullptr), children(new Node()), prev(this), next(this) {}
	Node(const pair<unsigned, Visited*>& d, Node* p, Node* n) : data(d), marked(false), degree(0), parent(nullptr), children(new Node()), prev(p), next(n) {}
	~Node();
};

struct Visited {
	unsigned index;
	unsigned changes;
	unsigned previndex;
	unsigned prevchanges;
	char trans; //0 tram, 1 bus
	char prevtrans;
	unique_ptr<Node> node;
	
	unsigned dist() { return node->data.first; }
// 	Visited() : /*state(0), */index(0), fp(nullptr), prevp(nullptr) {}
// 	Visited(unsigned i, unsigned c, unsigned pi, unsigned pc, Node* n) : /*state(s), */index(i), changes(c), previndex(pi), prevchanges(pc) {}
	Visited(unsigned i, unsigned c, unsigned pi, unsigned pc, char t, char pt, unsigned d) : index(i), changes(c), previndex(pi), prevchanges(pc), trans(t), prevtrans(pt), node(new Node(make_pair(d,this))) {}
	Visited(unsigned i, unsigned c, unsigned pi, unsigned pc, char t, char pt, unsigned d, FibHeap& fh);
};

class FibHeap
{
private:
	Node* roots;
	Node* min;
public:
	FibHeap() : roots(new Node()) , min(roots) {}
	Node* insert(const pair<unsigned, Visited*>& d);
	Node* insert(Node* n);
	Node* insert(unsigned key, Visited* v) { return insert(make_pair(key, v)); }
	bool decreaseKey(unique_ptr<Node>& np, unsigned nk); //true - sukces; false - nk > n.key
	//void deleteKey(Node* n); nie potrzebne, !! nie uzywac decreaseKey z -inf bo unsigned
	bool empty() { return roots->next == roots; }
	pair<unsigned, Visited*>& top() { return min->data; }
	void extractMin();
	Node* unlinkMin();
	~FibHeap();
};

#endif // FIBHEAP_H
