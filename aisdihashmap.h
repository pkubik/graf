/** 
@file aisdihashmap.h

AISDIHashMap and related functions interface.
	  
@author
Pawel Cichocki
Pawel Kubik

@date  
last revision
- 2014 Pawel Kubik: implemented methods
- 2006.03.24 Pawel Cichocki: wrote it

COPYRIGHT:
Copyright (c) 2006 Instytut Informatyki, Politechnika Warszawska
ALL RIGHTS RESERVED
*******************************************************************************/

#include <utility>
#include <iterator>
#include <cstring>

/// Default keys' comparing function for AISDIHashMap - it uses
/// operator== by default.
/// @returns true if keys are equal, false otherwise.
/// @param key1 First key to compare. 
/// @param key2 Second key to compare. 
#define ARRAYSIZE 65536
// #define LISTDEBUG

template <class Key>   
inline bool _compFunc(const Key& key1,const Key& key2)
{
	return (key1==key2);
}

inline unsigned hashF(const std::string& m)
{
	unsigned short h = 0;
	const char* str = m.c_str();
	char c;
    int i = 20;

	while ((c = *str++) && i--)
		h += (h << 2) + c;
#ifdef LISTDEBUG
    h %= 1;
#endif
	return h;
}
/// A map with a similar interface to std::map.
template<class K, class V, unsigned hashFunc(const K&), bool compFunc(const K&,const K&)=&_compFunc<K> >
class AISDIHashMap
{
    typedef AISDIHashMap<K,V,hashFunc,compFunc> HASHMAP;
public:
    typedef K key_type;
    typedef V value_type;
    typedef unsigned size_type;
    
private:
    typedef std::pair<key_type,value_type> DType;
    
    
    struct Node {
        DType data;
        Node* next;
        
        Node(const DType& d) : data(d), next(NULL) {}
        Node(const DType& d, Node* n) : data(d), next(n) {}
        Node(const Node& n) : data(n.data), next(NULL) {}
    };
    
    struct IteListNode {
        Node* node;
        IteListNode* prev;
        IteListNode* next;
        
        IteListNode() {}
        IteListNode(Node* n, IteListNode* p, IteListNode* nex) : node(n), prev(p), next(nex) {}
        void update(Node* n) { node = n; }
    };
    
    class IteList {
        IteListNode* first;
        IteListNode* last;
    public:
        IteList() {
            last = first = new IteListNode(NULL, NULL, NULL); //wartownik
        }
        
        ~IteList() {
            IteListNode* n = first;
            while(n) {
                IteListNode* d = n;
                n = n->next;
                delete d;
            }
        }
        
        IteListNode* add(Node* n) {
            last->next = new IteListNode(n, last, NULL);
            return last = last->next;
        }
        
        void erase(IteListNode* d) {
            d->prev->next = d->next;
            if(d->next)
                d->next->prev = d->prev;
            else
                last = d->prev;
            delete d;
        }
        
        IteListNode* back() const { return last; }
        IteListNode* front() const { return first->next; }
    };
    
    struct Header {
        Node* first;
        IteListNode* il;
        
        Header() {}
        Header(Node* f, IteListNode* i) : first(f), il(i) {}
    };
    
    Header* nodes[ARRAYSIZE];
    size_type m_size;
    IteList iterlist;
    
public:
    
    AISDIHashMap() : m_size(0)
    {
        memset(nodes, 0, ARRAYSIZE*sizeof(Header*));
    }
    
    ~AISDIHashMap()
    {
        clear();
    }

    /// Coping constructor.
    AISDIHashMap(const HASHMAP& a) : m_size(a.m_size)
    {
        memset(nodes, 0, ARRAYSIZE*sizeof(Header*));
        IteListNode* iln = a.iterlist.front();
        while(iln)
        {
            unsigned h = hashF(iln->node->data.first);
            Node* n = a.nodes[h]->first;
            nodes[h] = new Header();
            Node* m = nodes[h]->first = new Node(*n);
            while(n->next)
            {
                m->next = new Node(*(n->next));
                m = m->next;
                n = n->next;
            }
            iln = iln->next;
            nodes[h]->il = iterlist.add(nodes[h]->first);
        }
    }

    /// const_iterator.
    class const_iterator : public std::iterator<std::bidirectional_iterator_tag, std::pair<key_type, value_type> >
    {
        friend class AISDIHashMap;
        const HASHMAP* root;
    protected:
        Node* node;
        const_iterator(Node* n, const HASHMAP* h) : root(h), node(n) {}
    public:
        bool operator !=(const const_iterator& a) const {
            return node != a.node;
        }
        
        bool operator ==(const const_iterator& a) const {
            return node == a.node;
        }

        const value_type& operator*() const
        {
            return node->data;
        }
        
        const value_type* operator->() const
        {
            return &(node->data);
        }
        
        const_iterator& operator++()
        {
            if(node->next)
                node = node->next;
            else
            {
                unsigned h = hashF(node->data.first);
                if(root->nodes[h]->il->next)
                    node = root->nodes[h]->il->next->node;
                else
                    node = NULL;
            }
            return *this;
        }
        
        const_iterator operator++(int)
        {
            Node* tmp = node;
            if(node->next)
                node = node->next;
            else
            {
                unsigned h = hashF(node->data.first);
                if(root->nodes[h]->il->next)
                    node = root->nodes[h]->il->next->node;
                else
                    node = NULL;
            }
            return const_iterator(tmp, root);
        }
        
        const_iterator& operator--()
        {
            if(node)
            {
                unsigned h = hashF(node->data.first);
                if(node == root->nodes[h]->first)
                {
                    if(root->nodes[h]->il->prev->node)
                    {
                        node = root->nodes[h]->il->prev->node;
                        while(node->next)
                            node = node->next;
                    } //else this == begin -> nie cofaj
                }
                else
                {
                    Node* n = root->nodes[h]->first;
                    while(n->next != node)
                    {
                        n = n->next;
                    }
                    node = n;
                }
            }
            else // this == end()
            {
                node = root->iterlist.back()->node;
            }
            
            return *this;
        }
        
        const_iterator operator--(int)
        {
            Node* n = node;
            if(node)
            {
                unsigned h = hashF(node->data.first);
                if(node == root->nodes[h]->first)
                {
                    if(root->nodes[h]->il->prev->node)
                    {
                        node = root->nodes[h]->il->prev->node;
                        while(node->next)
                            node = node->next;
                    } //else this == begin -> nie cofaj
                }
                else
                {
                    Node* n = root->nodes[h]->first;
                    while(n->next != node)
                    {
                        n = n->next;
                    }
                    node = n;
                }
            }
            else // this == end()
            {
                node = root->iterlist.back()->node;
            }
            
            return const_iterator(n, this);
        }
        
        const_iterator() {}
        const_iterator(const const_iterator& cp) : root(cp.root), node(cp.node) {}
    };
    /// iterator.
    class iterator : public const_iterator 
    {
        friend class AISDIHashMap;
    protected:
        iterator(Node* n, const HASHMAP* h) : const_iterator(n, h) {}
    public:
        iterator() {}
        iterator(const iterator& cp) : const_iterator(cp) {}
        
        DType& operator*() const
        {
            return this->node->data;
        }
        
        DType* operator->() const
        {
            return &(this->node->data);
        }
        
        iterator& operator++()
        {  // preincrementacja
            ++(*(const_iterator*)this);
            return (*this);
        }
        
        iterator operator++(int)
        {  // postincrementacja
            iterator temp = *this;
            ++*this;
            return temp;
        }
        
        iterator& operator--()
        {  // predekrementacja
            --(*(const_iterator*)this);
            return (*this);
        }
        
        iterator operator--(int)
        {  // postdekrementacja
            iterator temp = *this;
            --*this;
            return temp;
        }
    };

    /// Returns an iterator addressing the first element in the map.
    inline iterator begin()
    {
        if(iterlist.front())
            return iterator(iterlist.front()->node, this);
        else
            return end();
    }
    inline const_iterator begin() const
    {
        if(iterlist.front())
            return const_iterator(iterlist.front()->node, this);
        else
            return end();
    }

    /// Returns an iterator that addresses the location succeeding the last element in a map.
    inline iterator end()
    {
        return iterator(NULL, this);
    }
    inline const_iterator end() const
    {
        return const_iterator(NULL, this);
    }

    /// Inserts an element into the map.
    /// @returns A pair whose bool component is true if an insertion was
    ///          made and false if the map already contained an element
    ///          associated with that key, and whose iterator component coresponds to
    ///          the address where a new element was inserted or where the element
    ///          was already located.
    std::pair<iterator, bool> insert(const std::pair<K, V>& entry)
    {
        int index = hashF(entry.first);
        Node* n;
        std::pair<iterator, bool> ret;
        if(nodes[index])
        {
            n = nodes[index]->first;
            if(n->data.first > entry.first)
            {
                nodes[index]->first = new Node(entry, n);
                ret.first = iterator(nodes[index]->first, this);
                ret.second = true;
            }
            else if(n->data.first == entry.first)
            {
                ret.first = iterator(nodes[index]->first, this);
                ret.second = false;
            }
            else
            {
                while(n->next && n->next->data.first < entry.first)
                {
                    n = n->next;
                }
                if(!n->next)
                {
                    n->next = new Node(entry);
                    ret.first = iterator(n->next, this);
                    ret.second = true;
                }
                else if(n->next->data.first == entry.first)
                {
                    ret.first = iterator(n->next, this);
                    ret.second = false;
                }
                else
                {
                    Node* nn = n->next;
                    n->next = new Node(entry, nn);
                    ret.first = iterator(n->next, this);
                    ret.second = true;
                }
            }
        }
        else
        {
            
            nodes[index] = new Header();
            nodes[index]->first = new Node(entry);
            nodes[index]->il = iterlist.add(nodes[index]->first);
            ret.first = iterator(nodes[index]->first, this);
            ret.second = true;
        }
        
        if(ret.second == true) {
            nodes[index]->il->update(nodes[index]->first);
            ++m_size;
        }
        
        return ret;
    }

    /// Returns an iterator addressing the location of the entry in the map
    /// that has a key equivalent to the specified one or the location succeeding the
    /// last element in the map if there is no match for the key.
    iterator find(const K& k)
    {
        int index = hashF(k);
        Node* n = NULL;
        if(nodes[index])
        {
            n = nodes[index]->first;
            while(n && n->data.first != k)
            {
                n = n->next;
            }
        }
        
        return iterator(n, this); // n=NULL <=> return end()
    }

    /// Returns an const iterator
    const_iterator find(const K& k) const
    {
        int index = hashF(k);
        Node* n = NULL;
        if(nodes[index])
        {
            n = nodes[index]->first;
            while(n && n->data.first != k)
            {
                n = n->next;
            }
        }
        
        return const_iterator(n, this); // n=NULL <=> return end()
    }

    /// Inserts an element into a map with a specified key value
    /// if one with such a key value does not exist.
    /// @returns Reference to the value component of the element defined by the key.
    V& operator[](const K& k)
    {
        return insert(std::make_pair(k, value_type())).first->second;
    }

    /// Tests if a map is empty.
    bool empty() const
    {
        return m_size == 0;
    }

    /// Returns the number of elements in the map.
    size_type size() const
    {
        return m_size;
    }

    /// Returns the number of elements in a map whose key matches a parameter-specified key.
    size_type count(const K& _Key) const
    {
        if(find(_Key) != end())
            return 1;
        else
            return 0;
    }

    /// Removes an element from the map.
    /// @returns The iterator that designates the first element remaining beyond any elements removed.
    iterator erase(iterator i)
{   
        unsigned h = hashF(i.node->data.first);
        Node* dn = i.node;
        if(i.node != nodes[h]->first)
        {
            --i;
            i.node->next = i.node->next->next;
            delete dn;
            i.node = i.node->next;
        }
        else
        {
            if(dn->next)
            {
                nodes[h]->first = dn->next;
                delete dn;
                nodes[h]->il->update(nodes[h]->first);
                i.node = nodes[h]->first;
            }
            else
            {
                iterlist.erase(nodes[h]->il);
                delete dn;
                delete nodes[h];
                nodes[h] = NULL;
            }
        }
        --m_size;
        return i;
    }

    /// Removes a range of elements from the map.
    /// The range is defined by the key values of the first and last iterators
    /// first is the first element removed and last is the element just beyond the last elemnt removed.
    /// @returns The iterator that designates the first element remaining beyond any elements removed.
    iterator erase(iterator first, iterator last)
    {
        while(first != last)
            first = erase(first);
        return last;
    }

    /// Removes an element from the map.
    /// @returns The number of elements that have been removed from the map.
    ///          Since this is not a multimap it should be 1 or 0.
    size_type erase(const K& key)
    {
        iterator i = find(key);
        if(i != end())
        {
            erase(i);
            return 1;
        }
        return 0;
    }

    /// Erases all the elements of a map.
    void clear()
    {
        int i=0;
        int j=0;
        IteListNode* iln = iterlist.front();
        while(iln)
        {
            i++;
            unsigned h = hashF(iln->node->data.first);
            Node* n = nodes[h]->first;
            while(n) 
            {
                j++;
                Node* tmp = n;
                n = n->next;
                delete tmp;
            }
            iln = iln->next;
            iterlist.erase(nodes[h]->il);
            delete nodes[h];
            nodes[h] = NULL;
        }
        m_size = 0;
    }


};
