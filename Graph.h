/*
 * Paweł Kubik
 * Politechnika Warszawska
 * WEiTI
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#include <string>
#include <vector>
#include <list>

using namespace std;

struct Output {
	unsigned v1;
	unsigned v2;
	char trans;
	unsigned dist;
	Output(unsigned a1, unsigned a2, char t, unsigned d) : v1(a1), v2(a2), trans(t), dist(d) {}
};

class Graph /*FINAL*/ {
private:
	struct Connection {
		unsigned int dest;
		unsigned int tram;
		unsigned int bus;
		Connection(unsigned int d, unsigned int t, unsigned int b) : dest(d), tram(t), bus(b) {}
		
		unsigned getDist(char c) { if(c%2) return bus; else return tram; } //f-cja pomocnicza
	};
    
    vector< list<Connection> > nodes;
public:
    static const unsigned inf;
    void addConnection(unsigned from, unsigned to, unsigned t, unsigned b);
    void solve(unsigned k, unsigned begin, unsigned end, list<Output>& output);
};

#endif /* GRAPH_H_ */
