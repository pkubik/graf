/*
* Paweł Kubik
* Politechnika Warszawska
* WEiTI
*/

#include <iostream>
#include <fstream>
#include <string>
#include "aisdihashmap.h"
#include "Graph.h"

using namespace std;

int main(int argc, char *argv[]) {
	if(argc == 3) {
		unsigned dest=1, k;
		typedef AISDIHashMap<string, unsigned int, hashF, _compFunc> HM;
		Graph graph;
		HM hm;
		vector<string> nodes;
		ifstream ifile(argv[1], ifstream::in);
		string buf;
		pair<HM::iterator, bool> insfb;
		pair<string, unsigned int> entry(string(), 0);
		
		ifile>>k;
		
		entry.first = "";
		do {
			ifile>>buf;
			entry.first.append(buf);
			if(buf.back()!='\"')
				entry.first.append(" ");
			else
				break;
		} while(true);
		entry.second = 0;
		hm.insert(entry);
		nodes.push_back(entry.first);
		
		entry.first = "";
		do {
			ifile>>buf;
			entry.first.append(buf);
			if(buf.back()!='\"')
				entry.first.append(" ");
			else
				break;
		} while(true);
		++entry.second;
		insfb = hm.insert(entry);
		if(insfb.second) {
			nodes.push_back(entry.first);
			//dest = 1
		}
		else
		{
			cout<<0;
			return 0;
		}
		
		while(ifile) {
			unsigned n1, n2, t, b;
			entry.first = "";
			do {
				ifile>>buf;
				entry.first.append(buf);
				if(buf.back()!='\"')
					entry.first.append(" ");
				else
					break;
			} while(true);
			
			if(!ifile) break; //hotfix
			
			entry.second = nodes.size();
			insfb = hm.insert(entry);
			if(insfb.second) {
				nodes.push_back(entry.first);
				n1 = entry.second;
			}
			else {
				n1 = insfb.first->second;
			}
			
			entry.first = "";
			do {
				ifile>>buf;
				entry.first.append(buf);
				if(buf.back()!='\"')
					entry.first.append(" ");
				else
					break;
			} while(true);
			entry.second = nodes.size();
			insfb = hm.insert(entry);
			if(insfb.second) {
				nodes.push_back(entry.first);
				n2 = entry.second;
			}
			else {
				n2 = insfb.first->second;
			}
			
			ifile>>t>>b;
			
			if(!t)
				t = Graph::inf;
			if(!b)
				b = Graph::inf;
			
			graph.addConnection(n1, n2, t, b);
		}
		
		ifile.close();
		list<Output> output;
		graph.solve(k,0,dest,output);
		
		ofstream ofile(argv[2], ifstream::out);
		for(auto it=output.begin();it!=output.end();++it) {
			ofile<<nodes[it->v1]<<" "<<nodes[it->v2]<<" "<<((it->trans)?'A':'T')<<" "<<it->dist<<"\n";
		}
	}
	else {
		cout<<"Specify correct I/O files as arguments.\n";
	}
	return 0;
}
