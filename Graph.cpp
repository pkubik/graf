/*
* Paweł Kubik
* Politechnika Warszawska
* WEiTI
*/

#include "Graph.h"
#include "FibHeap.h"
#include <map>

const unsigned Graph::inf = -1;

void Graph::addConnection(unsigned int from, unsigned int to, unsigned int t, unsigned int b)
{
	int dif = from - nodes.size();
	if(dif < int(to-nodes.size()) )
		dif = to - nodes.size();
	while(dif >= 0) { //index nie moze byc rowny size
		nodes.emplace_back();
		dif--;
	}
	nodes[from].push_back(Connection(to, t, b));
}

void genOutput(unsigned b, Visited* end,vector< map<unsigned, Visited> >* vis, list<Output>& output) {
	auto ins = vis[unsigned(end->prevtrans)][end->previndex].find(end->prevchanges);
	output.push_front(Output(end->previndex, end->index, end->trans, end->dist()-ins->second.dist()));
	
	auto inp = vis[unsigned(ins->second.prevtrans)][ins->second.previndex].find(ins->second.prevchanges);
	
	while(ins->second.index != b) {
		output.push_front(Output(ins->second.previndex, ins->second.index, ins->second.trans, ins->second.dist()-inp->second.dist()));
		ins = inp;
		inp = vis[unsigned(ins->second.prevtrans)][ins->second.previndex].find(ins->second.prevchanges);
	}
}

void Graph::solve(unsigned int k, unsigned int begin, unsigned int end, list< Output >& output)
{
	typedef list<Connection>::iterator iter;
	output.clear();
	FibHeap fh;
	vector< map<unsigned, Visited> > vis[] = {vector< map<unsigned, Visited> >(nodes.size()), vector< map<unsigned, Visited> >(nodes.size())};
	
	auto inp = vis[0][begin].insert(make_pair(0, Visited(0,0,0,0,0,0,0)));
	inp.first->second.node->data.second = &inp.first->second;
	inp = vis[1][begin].insert(make_pair(0, Visited(0,0,0,0,1,1,0)));
	inp.first->second.node->data.second = &inp.first->second;
	
	for(iter it = nodes[begin].begin();it!=nodes[begin].end();++it) {
		if(it->tram != inf) {
			inp = vis[0][it->dest].insert(make_pair(0, Visited(it->dest,0,0,0,0,0,it->tram, fh)));
			inp.first->second.node->data.second = &inp.first->second;
		}
		if(it->bus != inf) {
			inp = vis[1][it->dest].insert(make_pair(0, Visited(it->dest,0,0,0,1,1,it->bus, fh)));
			inp.first->second.node->data.second = &inp.first->second;
		}
	}
	
	Node* cn;
	Visited* cv;
	
	while(!fh.empty()) {
		cn = fh.unlinkMin();
		cv = cn->data.second;
		unsigned changes = cv->changes;
		unsigned d;
		unsigned char t = cv->trans;
		unsigned index = cv->index;
		
		if(index == end)
			break;
		
		for(iter it = nodes[index].begin();it!=nodes[index].end();++it) {
			if((d=it->getDist(t)) != inf) { // 1) brak przesiadki
				d+=cn->data.first;
				auto ins = vis[t][it->dest].upper_bound(changes);
				if(ins == vis[t][it->dest].begin()) {
					ins = vis[t][it->dest].insert(ins, make_pair(changes, Visited(it->dest, changes, index, changes, t, t, d,fh)));
					ins->second.node->data.second = &ins->second;
					
					++ins;
					while(ins != vis[t][it->dest].end() && ins->second.dist() >= d) {
						fh.decreaseKey(ins->second.node, 0);
						fh.extractMin();
						vis[t][it->dest].erase(ins++);
					}
				}
				else {
					--ins;
					if(ins != vis[t][it->dest].end() && ins->second.dist() > d) { //jesli nowy v ma mniejsza odleglosc niz pierwszy element o mniejszej liczbie przesiadek
						if(ins->first == changes) { //jesli juz istnieje element o takiej liczbie przesiadek
							fh.decreaseKey(ins->second.node, d);
							ins->second.previndex = index;
						}
						else {
							ins = vis[t][it->dest].insert(ins, make_pair(changes, Visited(it->dest, changes, index, changes, t, t, d,fh)));
							ins->second.node->data.second = &ins->second;
						}
						
						++ins;
						while(ins != vis[t][it->dest].end() && ins->second.dist() >= d) {
							fh.decreaseKey(ins->second.node, 0);
							fh.extractMin();
							vis[t][it->dest].erase(ins++);
						}
					}
				}
			}
			unsigned char t2 = (t+1)%2;
			if((d=it->getDist(t2)) != inf) {
				d+=cn->data.first;
				unsigned c2 = changes+1;
				if(c2 <= k) {
					//COPY-PASTE ALERT
					auto ins = vis[t2][it->dest].upper_bound(c2);
					if(ins == vis[t2][it->dest].begin()) {
						ins = vis[t2][it->dest].insert(ins, make_pair(c2, Visited(it->dest, c2, index, changes, t2, t, d,fh)));
						ins->second.node->data.second = &ins->second;
						
						++ins;
						while(ins != vis[t2][it->dest].end() && ins->second.dist() >= d) {
							fh.decreaseKey(ins->second.node, 0);
							fh.extractMin();
							vis[t2][it->dest].erase(ins++);
						}
					}
					else {
						--ins;
						if(ins != vis[t2][it->dest].end() && ins->second.dist() > d) { //jesli nowy v ma mniejsza odleglosc niz pierwszy element o mniejszej liczbie przesiadek
							if(ins->first == c2) { //jesli juz istnieje element o takiej liczbie przesiadek
								fh.decreaseKey(ins->second.node, d);
								ins->second.previndex = index;
							}
							else {
								ins = vis[t2][it->dest].insert(ins, make_pair(c2, Visited(it->dest, c2, index, changes, t2, t, d,fh)));
								ins->second.node->data.second = &ins->second;
							}
							
							++ins;
							while(ins != vis[t2][it->dest].end() && ins->second.dist() >= d) {
								fh.decreaseKey(ins->second.node, 0);
								fh.extractMin();
								vis[t2][it->dest].erase(ins++);
							}
						}
					}
				}
			}
		}
	}
	
	genOutput(begin, cv, vis, output);
}
