/*
* Paweł Kubik
* Politechnika Warszawska
* WEiTI
*/

#include <vector>
#include "FibHeap.h"

Visited::Visited(unsigned i, unsigned c, unsigned pi, unsigned pc, char t, char pt, unsigned d, FibHeap& fh) : index(i), changes(c), previndex(pi), prevchanges(pc), trans(t), prevtrans(pt), node(new Node(make_pair(d,this))) {
	fh.insert(node.get()); //juz zbedne
}

Node::~Node()
{
	if(children) {
// 		Node* it = children->next;
// 		Node* tmp;
// 		while(it!=children) {
// 			tmp = it;
// 			it = it->next;
// 			delete tmp;
// 		}
		delete children;
	}
}

Node* FibHeap::insert(const pair<unsigned, Visited*>& d)
{
	roots->prev = new Node(d, roots->prev, roots);
	roots->prev->prev->next = roots->prev;
	if(min->data.first > d.first)
		min = roots->prev;
	return roots->prev;
}

Node* FibHeap::insert(Node* n)
{
	n->next = roots;
	n->prev = roots->prev;
	roots->prev->next = n;
	roots->prev = n;
	if(min->data.first > n->data.first)
		min = n;
	return n;
}


void FibHeap::extractMin() {
	if(!empty()) {
		if(min->degree) { //splice
			Node* it = min->children->next;
			for(;it != min->children;it = it->next) {
				it->marked = false;
				it->parent = nullptr;
			}
			min->children->prev->next = min->next;
			min->prev->next = min->children->next;
			min->children->next->prev = min->prev;
			min->next->prev = min->children->prev;
			min->children->next = min->children; //zawin z jednej strony (i tak usuwamy)
		}
		else {
			min->next->prev = min->prev;
			min->prev->next = min->next;
		}
		
		delete min;
		
		min = roots;
		
		vector<Node*> deg;
		
		Node* it = roots->next;
		
		while(it != roots) {
			if(it->data.first < min->data.first) {
				min = it;
			}
			
			while(it->degree >= deg.size()) {
				deg.push_back(nullptr);
			}
			
			Node* tmp = it;
			it = it->next;
			
			while(deg[tmp->degree] && deg[tmp->degree] != tmp) {
				Node* les,* hig;
				if(deg[tmp->degree]->data.first < tmp->data.first) {
					les = deg[tmp->degree];
					hig = tmp;
				}
				else {
					les = tmp;
					hig = deg[tmp->degree];
				}
				hig->prev->next = hig->next;
				hig->next->prev = hig->prev;
				
				les->children->prev->next = hig;
				hig->prev = les->children->prev;
				les->children->prev = hig;
				hig->next = les->children;
				hig->parent = les;
				
				deg[les->degree] = nullptr;
				++les->degree;
				tmp = les;
				
				while(tmp->degree >= deg.size()) {
					deg.push_back(nullptr);
				}
			}
			
			deg[tmp->degree] = tmp;
			
		}
	}
}

Node* FibHeap::unlinkMin() {
	Node* ret = nullptr;
	
	if(!empty()) {
		
		if(min->degree) { //splice
			
			Node* it = min->children->next;
			
			for(;it != min->children;it = it->next) {
				it->marked = false;
				it->parent = nullptr;
			}
			
			min->children->prev->next = min->next;
			min->prev->next = min->children->next;
			min->children->next->prev = min->prev;
			min->next->prev = min->children->prev;
			min->children->next = min->children; //zawin z jednej strony (i tak usuwamy)
		}
		
		else {
			min->next->prev = min->prev;
			min->prev->next = min->next;
		}
		
		ret = min;
		
		min = roots;
		vector<Node*> deg;
		
		Node* it = roots->next;
		
		while(it != roots) {
			if(it->data.first < min->data.first) {
				min = it;
			}
			
			while(it->degree >= deg.size()) {
				deg.push_back(nullptr);
			}
			
			Node* tmp = it;
			it = it->next;
			
			while(deg[tmp->degree] && deg[tmp->degree] != tmp) {
				Node* les,* hig;
				
				if(deg[tmp->degree]->data.first < tmp->data.first) {
					les = deg[tmp->degree];
					hig = tmp;
				}
				
				else {
					les = tmp;
					hig = deg[tmp->degree];
				}
				
				hig->prev->next = hig->next;
				hig->next->prev = hig->prev;
				les->children->prev->next = hig;
				hig->prev = les->children->prev;
				les->children->prev = hig;
				hig->next = les->children;
				hig->parent = les;
				deg[les->degree] = nullptr;
				
				++les->degree;
				tmp = les;
				
				while(tmp->degree >= deg.size()) {
					deg.push_back(nullptr);
				}
			}
			
			deg[tmp->degree] = tmp;
		}
	}
	
	return ret;
}


bool FibHeap::decreaseKey(unique_ptr<Node>& np, unsigned int nk)
{
	Node* n = np.get();
	
	if(nk >= n->data.first)
		return false;
	else {
		n->data.first = nk;
		if(n->parent) {
			n->next->prev = n->prev;
			n->prev->next = n->next;
			Node* p = n->parent;
			n->parent = nullptr;
			--p->degree;
			insert(n);
			while(p && p->marked) {
				n = p;
				n->marked = false;
				n->next->prev = n->prev;
				n->prev->next = n->next;
				p = n->parent;
				n->parent = nullptr;
				insert(n);
			}
			
			if(p)
				p->marked = true;
		}
		else {
			if(min->data.first > n->data.first)
				min = n;
		}
		
		return true;
	}
}


FibHeap::~FibHeap() {
// 	Node* tmp;
// 	Node* n = roots->next;
// 	while(n != roots) {
// 		tmp = n;
// 		n = n->next;
// 		delete tmp;
// 	}
	delete roots;
}

